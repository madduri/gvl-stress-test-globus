#!/usr/bin/env python
import sys
import argparse
import string
import os

DEFAULT_TOTAL_THREADS = 1  # If no value specified on command line as first argument

if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-l", "--log", default="gvltest", help="Prefix for log files.")
	parser.add_argument("-b", "--browsers", type=int, default=DEFAULT_TOTAL_THREADS, help="Number of parallel browser threads to execute. Default is %s" % DEFAULT_TOTAL_THREADS)
	
	args, unknown = parser.parse_known_args()
	os.system("rm %s*.log" % args.log)
	
#	assert(num_threads >= 1 and num_threads < 100)
#    assert(log_file_name)
#    threads = []
#    for x in range(0, num_threads):
#        t = Thread(target=run_test, args=[x, log_file_name, args.grid, args.server, args.user, args.password, args.timeout, args.testclass])
#        t.start()
#        time.sleep(random.randint(0, args.delay))
#        threads.append(t)

#   for x in range(0, num_threads):
#        threads[x].join()


	os.system("python gvl_test_runner.py %s" % string.join(unknown, " "))
	os.system("python gvl_test_stats.py -l %s" % args.log)
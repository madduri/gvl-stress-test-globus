from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from gvl_test_base import GVLTestBase
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.rna_analysis import RNAAnalysis
from selenium_snippets.galaxy.sort_filter import SortFilter
from selenium_snippets.galaxy.ngs_mapping import NGSMapping


class VariantDetectionAdvanced(GVLTestBase):

    def __init__(self, galaxy_test_context):
        super(VariantDetectionAdvanced, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def execute_gvl_testcase(self):
        # self.run_tute_section1()
        self.run_tute_section2()

    @snippet_base.ui_action()
    def run_tute_section1(self):
        driver = self.driver
        History(self.context).import_history("variantDetection_ADVANCED_Prep")

    @snippet_base.ui_action()
    def run_tute_section2(self):
        NGSMapping(self.context).run_bwa_illumina("built-in", "hg19", paired_end=True, fastq1="NA12878.hiseq.wgs_chr20_2mb.30xPE.fastq_1", fastq2="NA12878.hiseq.wgs_chr20_2mb.30xPE.fastq_2",
                                                  extra_settings={ "read_group" : { "id" : "Tutorial_readgroup",
                                                                                   "library" : "Tutorial_library",
                                                                                   "platform": "ILLUMINA",
                                                                                   "sample": "NA12878"}
                                                                  })
        # self.verify_section2_output()
        pass

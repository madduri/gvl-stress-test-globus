from gvl_test_base import GVLOAuthTestBase
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.get_data import GetGlobusData
from selenium_snippets.galaxy.workflow import Workflow
from selenium_snippets.galaxy import snippet_base

GLOBUS_DATASET = {
    'endpoint': 'sulakhe#SequencingCenter',
    'dir': 'Exome-seq-sample-data',
    'files': (
        '1000G_biallelic.indels.hg19.vcf',
        'Exome-Sample_Forward_1.fastqsanger',
        'Exome-Sample_Reverse_2.fastqsanger',
        'dbsnp_132.hg19.vcf'
    )
}

WORKFLOW_NAME = "Illumina Complete Exome Analysis Pipeline"


class WorkflowDeseqBasic(GVLOAuthTestBase):

    def __init__(self, galaxy_test_context):
        super(WorkflowDeseqBasic, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def execute_gvl_testcase(self):
        self.get_globus_test_data()
        self.run_existing_workflow()
        self.verify_output()

    @snippet_base.ui_action()
    def get_globus_test_data(self):
        driver = self.driver
        if History(self.context).copy_history('GlobusData') == False:
            History(self.context).create_new_history('GlobusData')
            globus_dir = GLOBUS_DATASET['dir']
            endpoint = GLOBUS_DATASET['endpoint']
            for globus_file in GLOBUS_DATASET['files']:
                GetGlobusData(self.context).browse_and_get_data(endpoint, globus_dir, globus_file, 'genomics', 'globus', "1")
            #History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def run_existing_workflow(self):
        workflow = Workflow(self.context)
        form_fill_selects = [
            '2: Exome-Sample_Forw..fastqsanger',
            '3: Exome-Sample_Reve..fastqsanger',
            '4: dbsnp_132.hg19.vcf',
            '1: 1000G_biallelic.i..ls.hg19.vcf',
            '4: dbsnp_132.hg19.vcf',
            '4: dbsnp_132.hg19.vcf',
        ]
        form_fill_inputs = [
            'Test',
            'Test',
            'Test',
            'Test',
            '100',
            'hg19',
            'hg19',
            'hg19',
            'hg19',
            'hg19',
            'hg19',
        ]
        workflow.run_workflow(WORKFLOW_NAME, form_fill_selects, form_fill_inputs)
        #History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def verify_output(self):
        driver = self.driver
        self.switch_to_galaxy_content_frame()
        elem = driver.find_element_by_xpath("//div[@class='donemessagelarge']")
        if not elem.text.startswith('Successful'):
            raise Exception('Workflow failed')


from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from gvl_test_base import GVLTestBase
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.get_data import GetData
from selenium_snippets.galaxy.workflow import Workflow
from selenium_snippets.galaxy import snippet_base


INPUT_FILE1 = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/Assembly/ERR048396_1.fastq"
INPUT_FILE2 = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/Assembly/ERR048396_2.fastq"
INPUT_FILE3 = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/Assembly/illumina_adapters.fna"
WORKFLOW_URL = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/galaxy_tools_tests/test_workflow_for_denovo_genome_assembly_tut"
WORKFLOW_NAME = "microbial assembly"
HISTORY_NAME = "TestBot: Denovo Microbial Assembly Tutorial"
EXPECTED_OUTPUT = "ID lgth short1_cov\n"\
                  "1 849 512.181390\n"\
                  ">NODE_1_length_849_cov_512.181396\n"\
                  "GATAAAGTCCGTATAATTGTGTAAAAGTAAAAAGGCCATATAACAGTCCTTTTACGGTAC"

class WorkflowMicrobialAssembly(GVLTestBase):

    def __init__(self, galaxy_test_context):
        super(WorkflowMicrobialAssembly, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def execute_gvl_testcase(self):
        self.import_test_data()
        self.import_and_run_workflow()
        self.verify_output()

    @snippet_base.ui_action()
    def import_test_data(self):
        driver = self.driver
        History(self.context).create_new_history(HISTORY_NAME)
        GetData(self.context).run_upload_file('url', INPUT_FILE1 + ".gz", 'fastqsanger')
        GetData(self.context).run_upload_file('url', INPUT_FILE2 + ".gz", 'fastqsanger')
        GetData(self.context).run_upload_file('url', INPUT_FILE3)
        History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def import_and_run_workflow(self):
        workflow = Workflow(self.context)
        workflow.import_workflow('url', WORKFLOW_URL)
        workflow.run_workflow(WORKFLOW_NAME, [INPUT_FILE1, INPUT_FILE2, INPUT_FILE3])
        History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def verify_output(self):
        driver = self.driver
        History(self.context).view_last_history_item()
        self.switch_to_galaxy_content_frame()
        if not driver.find_element_by_xpath("//body/pre").text == EXPECTED_OUTPUT:
            raise Exception("Expected output:\n" + EXPECTED_OUTPUT + "\n\nbut found:\n" + driver.find_element_by_xpath("//body/pre").text)


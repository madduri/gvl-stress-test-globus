from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from gvl_test_base import GVLTestBase
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.rna_analysis import RNAAnalysis
from selenium_snippets.galaxy.sort_filter import SortFilter
from selenium_snippets.galaxy import snippet_base

class RNAseqDGEBASICPrep(GVLTestBase):

    def __init__(self, galaxy_test_context):
        super(RNAseqDGEBASICPrep, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def execute_gvl_testcase(self):
        History(self.context).import_history("RNAseqDGE_BASIC_Prep")
        self.run_tute_section1()
        self.run_tute_section2()
        self.run_tute_section3()

    @snippet_base.ui_action()
    def run_tute_section1(self):
        driver = self.driver
        rna_analysis = RNAAnalysis(self.context)
        ref_genome = "D melanogaster (dm3)"
        rna_analysis.run_tophat("1: C1_R1_1.chr4.fq", ref_genome)
        rna_analysis.run_tophat("2: C1_R2_1.chr4.fq", ref_genome)
        rna_analysis.run_tophat("3: C1_R3_1.chr4.fq", ref_genome)
        rna_analysis.run_tophat("4: C2_R1_1.chr4.fq", ref_genome)
        rna_analysis.run_tophat("5: C2_R2_1.chr4.fq", ref_genome)
        rna_analysis.run_tophat("6: C2_R3_1.chr4.fq", ref_genome)

        # Wait for tophat to finish
        self.clock_action('tophat run complete', 1001)
        History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def run_tute_section2(self):
        c1 = ["Tophat for Illumina on data 1: accepted_hits", "Tophat for Illumina on data 2: accepted_hits", "Tophat for Illumina on data 3: accepted_hits"]
        c2 = ["Tophat for Illumina on data 4: accepted_hits", "Tophat for Illumina on data 5: accepted_hits", "Tophat for Illumina on data 6: accepted_hits"]
        RNAAnalysis(self.context).run_cuffdiff([{'name' : 'C1', 'replicates': c1},
                                                {'name' : 'C2', 'replicates': c2}])
        SortFilter(self.context).run_sort_filter("gene differential expression testing", "c14=='yes'")
        History(self.context).wait_for_datasets_to_finish()
        self.verify_section2_output()

    @snippet_base.ui_action()
    def run_tute_section3(self):
        RNAAnalysis(self.context).run_cuffdiff([{'name' : 'C1', 'replicates': ["Tophat for Illumina on data 1: accepted_hits"]},
                                                {'name' : 'C2', 'replicates': ["Tophat for Illumina on data 4: accepted_hits"]}],
                                               normalization_method='classic-fpkm', dispersion_method='blind')
        SortFilter(self.context).run_sort_filter("gene differential expression testing", "c14=='yes'")
        History(self.context).wait_for_datasets_to_finish()
        self.verify_section3_output()

    @snippet_base.ui_action()
    def verify_section2_output(self):
        driver = self.driver
        self.clock_action("Section 2 View data click", 1001)
        History(self.context).view_last_history_item()

        self.switch_to_galaxy_content_frame()
        if not (self.is_element_present("id('content_table')//tr[contains(., 'Ank') and contains(., 'chr4:137014-150378') and contains(., '0.00175')]") and
           self.is_element_present("id('content_table')//tr[contains(., 'CG2177') and contains(., 'chr4:331557-334534') and contains(., '0.00175')]")):
            raise Exception("Expected output 'Ank' gene from chr4:137014-150378 with q-value 0.00175 and 'CG2177' from chr4:331557-334534 with q-value 0.00175")

    @snippet_base.ui_action()
    def verify_section3_output(self):
        driver = self.driver
        self.clock_action("Section 3 View data click", 1001)
        History(self.context).view_last_history_item()

        self.switch_to_galaxy_content_frame()
        if self.is_element_present("id('content_table')//tr[normalize-space()]"):
            raise Exception("Expected empty output but found differently expressed genes")

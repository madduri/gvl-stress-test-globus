from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets import util
from selenium_snippets.galaxy import snippet_base

class Workflow(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(Workflow, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def import_workflow(self, type, url):
        driver = self.driver
        self.switch_to_galaxy_outer_frame()
        self.clock_action('Workflow page load', 1001)
        driver.find_element_by_link_text("Workflow").click()
        self.clock_action('Import workflow page load', 1001)
        driver.find_element_by_xpath("id('center')//ul/li/a[contains(@href, '/workflow/import_workflow')]").click()
        self.clock_action('Import workflow begin: ' + url + ' page load', 1001)

        if type == 'url':
            driver.find_element_by_name("url").clear()
            driver.find_element_by_name("url").send_keys(url)
        elif type == 'file':
            driver.find_element_by_name("file_data").send_keys(url)

        driver.find_element_by_name("import_button").click()

    @snippet_base.ui_action()
    def run_workflow(self, name, select_list, input_list):
        driver = self.driver
        self.switch_to_galaxy_outer_frame()
        self.clock_action('Workflow page load', 1001)
        driver.find_element_by_link_text("Workflow").click()
        self.clock_action('Begin run workflow page load', 1001)
        driver.find_element_by_xpath("id('center')//table//div[contains(@id, 'popup')][contains(., '" + name + "')]").click()
        driver.find_element_by_link_text("Run").click()
        self.clock_action('Complete Run workflow page load', 1001)
        self.switch_to_galaxy_content_frame()

        input_elems = driver.find_elements_by_xpath("id('tool_form')//div[@class='toolForm']//input[@type!='hidden']")
        for idx, elem in enumerate(input_elems):
            elem.clear()
            elem.send_keys(input_list[idx])

        select_elems = driver.find_elements_by_xpath("id('tool_form')//div[@class='toolForm']//select")
        for idx, elem in enumerate(select_elems):
            select = Select(elem)
            select.select_by_visible_text(select_list[idx])

        self.clock_action('Workflow submit', 1001)
        elem = driver.find_element_by_name("run_workflow")
        elem.click()
        self.clock_action('Complete Workflow submit', 1001)

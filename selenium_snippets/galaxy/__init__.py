from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class GalaxySnippetContext():
    selenium_server = ""
    galaxy_server = "http://galaxy-tut.genome.edu.au/"
    galaxy_username = "testbot"  # = "<username>"
    galaxy_email = "testbot@genome.edu.au"  # = "<username>"
    galaxy_password = "gvl_letmein"  # = "<password>"
    job_timeout = 1800  # Maximum time (in seconds) to wait for job to finish execution
    page_timeout = 60  # Maximum time (in seconds) to wait for page to be ready
    current_timeout = page_timeout

    def initialize(self):
        if self.selenium_server:
            self.driver = WebDriver(command_executor=self.selenium_server, desired_capabilities=DesiredCapabilities.FIREFOX, browser_profile=None, proxy=None)
#             self.driver = WebDriver(command_executor=self.selenium_server, desired_capabilities={'platform': 'ANY'})
        else:
            self.driver = webdriver.Firefox()
#             self.driver = webdriver.PhantomJS(executable_path='/users/nuwan/versi/vlsci/gvl-stress-test/phantomjs-1.9.7-macosx/bin/phantomjs', port=8000)
        self.driver.delete_all_cookies()
        self.driver.set_window_size(1024, 768)
        self.driver.implicitly_wait(self.current_timeout)
        self.base_url = self.galaxy_server
        self.verificationErrors = []
        self.accept_next_alert = True
        self.timing = []
        self.current_action = self.timing
        self.statistics = {}
        self.error_count = 0
        self.top_window = self.driver.window_handles[0]

import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base
import logging

log = logging.getLogger(__name__)

class History(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(History, self).__init__(galaxy_test_context)

    @snippet_base.ui_action(snippet_base.ActionType.JOB_EXECUTE)
    def wait_for_datasets_to_finish(self):
        self.switch_to_galaxy_outer_frame()
        time.sleep(5)
        self.driver.implicitly_wait(5)  # Wait at least 5 seconds for the history panel to reload itself via ajax
        self.driver.find_element_by_id("history-refresh-button").click()
        # TODO: For backward compatibility. Remove references to galaxy_history frame when stable
        if self.is_element_present("//iframe[@name='galaxy_history']"):
            self.driver.switch_to_frame('galaxy_history')
        # WebDriverWait(self.driver, self.page_timeout).until_not(EC.presence_of_element_located((By.XPATH, "//div[@class='datasets-list']//div[contains(@class, 'state-running') or contains(@class, 'state-queued')]")))
        # TODO: The following line should be deprecated in favour of the line above once the Galaxy release become final. The line below accounts for dev/stable differences, but is a bit less robust.
        WebDriverWait(self.driver, self.context.job_timeout).until(lambda driver: not self.is_element_present("//div[contains(@class, 'datasets-list')]//div[contains(@class, 'running') or contains(@class, 'queued')]"))
        self.driver.implicitly_wait(self.context.page_timeout)

    @snippet_base.ui_action()
    def import_history(self, history_name):
        driver = self.driver
        self.switch_to_galaxy_outer_frame()
        self.clock_action('published histories page load', 1001)
        driver.find_element_by_link_text("Shared Data").click()
        driver.find_element_by_link_text("Published Histories").click()
        self.clock_action(history_name + ' page load', 1001)
        driver.find_element_by_link_text(history_name).click()
        self.clock_action('Import history page load', 1001)
        driver.find_element_by_link_text("Import history").click()
        self.clock_action('start using history page load', 1001)
        driver.find_element_by_link_text("start using this history").click()

    @snippet_base.ui_action()
    def import_history_archive(self, archive_url):
        driver = self.driver
        self.clock_action('import history file page load', 1001)
        self.switch_to_galaxy_outer_frame()
        driver.find_element_by_id("history-options-button").click()
        driver.find_element_by_link_text("Import from File").click()
        self.clock_action('submit history file page load', 1001)
        self.wait_for_galaxy_content_frame()
        driver.find_element_by_name("archive_url").click()
        driver.find_element_by_name("archive_url").clear()
        driver.find_element_by_name("archive_url").send_keys(archive_url)
        driver.find_element_by_name("archive_url").submit()
        driver.find_element_by_xpath("//div[contains(., 'Importing history from')]").click()

    @snippet_base.ui_action()
    def publish_history_archive(self, history_name):
        driver = self.driver
        self.refresh_till_import_complete(history_name)
        self.clock_action('rename history page load', 1001)
        # driver.find_element_by_xpath("//div[contains(., 'imported from archive: " + history_name + "')]").click()
        driver.find_element_by_xpath("//div[contains(., '" + history_name + "')]").click()
        driver.find_element_by_link_text("Rename").click()
        self.clock_action('complete history rename page load', 1001)
        driver.find_element_by_name("name").click()
        driver.find_element_by_name("name").clear()
        driver.find_element_by_name("name").send_keys(history_name)
        driver.find_element_by_name("history_rename_btn").click()
        # driver.find_element_by_xpath("//div[contains(., 'History: imported from archive: " + history_name + "')]").click()
        driver.find_element_by_xpath("//div[contains(., '" + history_name + "')]").click()
        self.load_saved_histories()
        self.clock_action('publish history page load', 1001)
        driver.find_element_by_xpath("//div[contains(., '" + history_name + "')]").click()
        driver.find_element_by_link_text("Share or Publish").click()
        self.clock_action('complete publish history page load', 1001)
        driver.find_element_by_name("make_accessible_and_publish").click()
        driver.find_element_by_xpath("//div[contains(., 'This history is currently accessible via link and published')]").click()

    @snippet_base.ui_action()
    def load_saved_histories(self):
        driver = self.driver
        self.clock_action('saved histories page load', 1001)
        self.switch_to_galaxy_history_frame()
        driver.find_element_by_id("history-options-button").click()
        driver.find_element_by_link_text("Saved Histories").click()
        self.switch_to_galaxy_outer_frame()
        self.wait_for_galaxy_content_frame()

    @snippet_base.ui_action()
    def refresh_till_import_complete(self, history_name):
        self.load_saved_histories()
        while not self.is_element_present("//div[contains(., 'imported from archive:')][contains(., '" + history_name + "')]"):
            time.sleep(3)
            self.load_saved_histories()

    @snippet_base.ui_action()
    def view_last_history_item(self):
        self.switch_to_galaxy_outer_frame()
        self.driver.find_element_by_id("history-refresh-button").click()
        time.sleep(5)
        # TODO: For new Galaxy release, replace the line below with the one above, since history frame is obsolete
        self.switch_to_galaxy_history_frame()
        # driver.find_element_by_xpath("id('current-history-panel')//div[contains(@class, 'dataset-primary-actions')]/a[contains(@data-original-title, 'View data')]").click()
        # TODO: For new Galaxy release, replace the line below with the one above, since the one below is less efficient.
        self.driver.find_element_by_xpath("//a[contains(@data-original-title, 'View data')]").click()
        self.switch_to_galaxy_outer_frame()
        self.wait_for_galaxy_content_frame()

    @snippet_base.ui_action()
    def wait_for_history_item(self, name):
        driver = self.driver
        self.switch_to_galaxy_outer_frame()
        self.driver.find_element_by_id("history-refresh-button").click()
        self.switch_to_galaxy_history_frame()
        driver.find_element_by_xpath("//div[@class='dataset-primary-actions'][following-sibling::div[contains(., '" + name + "')]]").click()
        self.switch_to_galaxy_outer_frame()
        self.wait_for_galaxy_content_frame()

    @snippet_base.ui_action()
    def edit_history_item(self, name):
        driver = self.driver
        self.clock_action('Edit history attributes page load', 1001)
        self.switch_to_galaxy_history_frame()
        driver.find_element_by_xpath("//div[@class='dataset-primary-actions'][following-sibling::div[contains(., '" + name + "')]]//a[contains(@data-original-title, 'Edit attributes')]").click()
        self.switch_to_galaxy_outer_frame()
        self.wait_for_galaxy_content_frame()

    @snippet_base.ui_action()
    def rename_history_item(self, current_name, new_name):
        driver = self.driver
        self.edit_history_item(current_name)
        self.switch_to_galaxy_content_frame()
        driver.find_element_by_name("name").clear()
        driver.find_element_by_name("name").send_keys(new_name)
        driver.find_element_by_name("save").click()
        self.wait_for_history_item(new_name)


    @snippet_base.ui_action()
    def copy_history(self, name):
        driver = self.driver
        self.switch_to_galaxy_outer_frame()
        driver.find_element_by_xpath("id('history-options-button')").click()
        driver.find_element_by_xpath("id('history-options-button-menu')/li/a[contains(., 'Saved Histories')]").click()

        self.switch_to_galaxy_content_frame()

        if self.is_element_present("//div[a[text()='GlobusData']]"):
            driver.find_element_by_xpath("//div[a[text()='GlobusData']]").click()
            driver.find_element_by_xpath("//li//a[contains(.,'Switch')]").click()

            self.switch_to_galaxy_outer_frame()
            driver.find_element_by_xpath("id('history-options-button')").click()
            driver.find_element_by_xpath("id('history-options-button-menu')/li/a[contains(., 'Copy History')]").click()
            self.switch_to_galaxy_content_frame()
            driver.find_element_by_xpath("//input[@value='active']").click()
            driver.find_element_by_xpath("//input[@value='Copy']").click()

            WebDriverWait(self.driver, self.context.page_timeout).\
                until(lambda driver: self.is_element_present("//div[@class='donemessagelarge']//a"))
            driver.find_element_by_xpath("//div[@class='donemessagelarge']//a").click()

            return True
        else:
            return False



    @snippet_base.ui_action()
    def create_new_history(self, name=None):
        driver = self.driver
        self.switch_to_galaxy_outer_frame()
        driver.find_element_by_xpath("id('history-options-button')").click()
        driver.find_element_by_xpath("id('history-options-button-menu')/li/a[contains(., 'Create New')]").click()

        self.switch_to_galaxy_history_frame()

        WebDriverWait(self.driver, self.context.page_timeout).\
            until(lambda driver: self.is_element_present("id('history-controls')//div[text()='Unnamed history']"))

        self.switch_to_galaxy_outer_frame()
        driver.find_element_by_xpath("id('history-options-button')").click()
        driver.find_element_by_xpath("id('history-options-button-menu')/li/a[contains(., 'Saved Histories')]").click()

        self.switch_to_galaxy_content_frame()
        driver.find_element_by_xpath("id('grid-0-popup')").click()
        driver.find_element_by_xpath("id('grid-0-popup-menu')//li//a[contains(.,'Rename')]").click()

        elem = driver.find_element_by_name('name')
        elem.clear()
        elem.send_keys(name)

        driver.find_element_by_name('history_rename_btn').click()

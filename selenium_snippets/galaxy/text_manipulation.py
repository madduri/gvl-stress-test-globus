from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base


class TextManipulation(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(TextManipulation, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_textutil", "Cut")
    def run_cut(self, cut_columns, delimiter, input_file):
        driver = self.driver
        self.clock_action("text manip>cut run btn click", 1001)
        self.switch_to_galaxy_content_frame()

        driver.find_element_by_name("columnList").clear()
        driver.find_element_by_name("columnList").send_keys(cut_columns)
        Select(driver.find_element_by_name("delimiter")).select_by_visible_text(delimiter)
        driver.find_element_by_xpath("id('s2id_autogen1')/a[contains(@class, 'select2-choice')]").click()
        driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + input_file + "')][last()]").click()

        driver.find_element_by_name("runtool_btn").click()

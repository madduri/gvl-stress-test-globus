from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base

class SortFilter(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(SortFilter, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_filter", "Filter")
    def run_sort_filter(self, name, condition, lines_to_skip=None):
        driver = self.driver

        self.clock_action("Filter run btn click", 1001)
        self.switch_to_galaxy_content_frame()
        driver.find_element_by_xpath("//div/a[contains(@class, 'select2-choice')]").click()
        driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + name + "')][last()]").click()
        driver.find_element_by_name("cond").clear()
        driver.find_element_by_name("cond").send_keys(condition)

        if lines_to_skip:
            driver.find_element_by_name("header_lines").clear()
            driver.find_element_by_name("header_lines").send_keys(lines_to_skip)

        driver.find_element_by_name("runtool_btn").click()

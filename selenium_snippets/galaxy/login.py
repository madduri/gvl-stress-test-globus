from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base

class Login(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(Login, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def register_and_login(self, galaxy_url, galaxy_username, galaxy_email, galaxy_password):
            driver = self.driver
            self.clock_action('home page load', 1001)
            driver.get(galaxy_url)
            self.clock_action('registration page load', 1001)
            driver.find_element_by_xpath("id('user')/li/a").click()
            driver.find_element_by_link_text("Register").click()
            self.clock_action('registration process complete', 1001)
            self.switch_to_galaxy_content_frame()
            driver.find_element_by_name("email").click()
            driver.find_element_by_name("email").clear()
            driver.find_element_by_name("email").send_keys(galaxy_email)
            driver.find_element_by_name("password").clear()
            driver.find_element_by_name("password").send_keys(galaxy_password)
            driver.find_element_by_name("confirm").clear()
            driver.find_element_by_name("confirm").send_keys(galaxy_password)
            driver.find_element_by_name("username").clear()
            driver.find_element_by_name("username").send_keys(galaxy_username)
            driver.find_element_by_name("create_user_button").click()
            # Login only if user has been freshly created.
            if self.is_element_present("//div[@class='errormessage']"):
                self.switch_to_galaxy_outer_frame()
                self.clock_action('login page load', 1001)
                driver.find_element_by_link_text("User").click()
                driver.find_element_by_link_text("Login").click()
                self.clock_action('login process complete', 1001)
                self.switch_to_galaxy_content_frame()
                driver.find_element_by_name("email").click()
                driver.find_element_by_name("email").clear()
                driver.find_element_by_name("email").send_keys(galaxy_email)
                driver.find_element_by_name("password").clear()
                driver.find_element_by_name("password").send_keys(galaxy_password)
                driver.find_element_by_name("login_button").click()
                self.wait_for_galaxy_content_frame()
            else:
                driver.find_element_by_link_text("Return to the home page.").click()
            self.switch_to_galaxy_outer_frame()


class GOAuthLogin(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(GOAuthLogin, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def login(self, galaxy_url, galaxy_username, galaxy_password):
            driver = self.driver
            self.clock_action('home page load', 1001)
            driver.get(galaxy_url)
            self.clock_action('registration page load', 1001)

            driver.find_element_by_link_text("Please authenticate using globus online").click()
            driver.find_element_by_name("username").clear()
            driver.find_element_by_name("username").send_keys(galaxy_username)
            driver.find_element_by_name("password").clear()
            driver.find_element_by_name("password").send_keys(galaxy_password)
            driver.find_element_by_css_selector("button.signin_myproxy_submit.glbs_primary_button").click()
            driver.find_element_by_xpath("(//button[@name='next'])[2]").click()

            self.switch_to_galaxy_outer_frame()


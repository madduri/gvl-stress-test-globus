from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base
from selenium.webdriver.support.ui import WebDriverWait

class GetData(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(GetData, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_getext", "Upload File")
    def run_upload_file(self, type, file_url, file_format=None):
        """Get Data->Upload file.
        Returns nothing
    
        :param type: Maybe either 'file', 'url' or 'text'
        :param file_url: Path to the file if type 'file', full url if type 'url' and context text if type 'text'
        :param file_format: (optional) Format of file.
    
        Usage::
    
          >>> from selenium_snippets.galaxy.get_data import GetData
          >>> req = GetData(self.context).run_upload_file('url', 'https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/variantCalling_BASIC/NA12878.GAIIx.exome_chr22.1E6reads.76bp.fastq')
        """

        driver = self.driver

        self.clock_action("Upload File btn click", 1001)
        self.switch_to_galaxy_content_frame()

        if type == 'file':
            driver.find_element_by_name("files_0|file_data").clear()
            driver.find_element_by_name("files_0|file_data").send_keys(file_url)
        elif type == 'url' or type == 'text':
            driver.find_element_by_name("files_0|url_paste").clear()
            driver.find_element_by_name("files_0|url_paste").send_keys(file_url)

        if file_format:
            driver.find_element_by_xpath("id('s2id_autogen1')/a[contains(@class, 'select2-choice')]").click()
            driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + file_format + "')][last()]").click()

        driver.find_element_by_name("runtool_btn").click()


class GetGlobusData(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(GetGlobusData, self).__init__(galaxy_test_context)

    def is_authorized(self, username, password, timeout):
        path = "id('askpasswd')"
        if self.is_element_visible(path):
            driver = self.driver

            path = "id('epauth_user')"
            elem = driver.find_element_by_xpath(path)
            elem.clear()
            elem.send_keys(username)

            path = "id('epauth_passwd')"
            elem = driver.find_element_by_xpath(path)
            elem.clear()
            elem.send_keys(password)

            path = "id('epauth_lifetime')"
            elem = driver.find_element_by_xpath(path)
            elem.clear()
            elem.send_keys(timeout)

            path = "id('askpasswd')//form//input[@type='submit']"
            elem = driver.find_element_by_xpath(path)
            elem.click()

        return True

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_globus", "Browse and Get Data via Globus Online")
    def browse_and_get_data(self, endpoint, globus_dir, globus_file, username, password, timeout):
        driver = self.driver

        self.clock_action("Browse and Get Data via Globus Online btn click", 1001)
        self.switch_to_galaxy_content_frame()

        path = "id('s2id_autogen1')//a"
        driver.find_element_by_xpath(path).click()

        path = "id('select2-drop')//div//input"
        WebDriverWait(self.driver, self.context.page_timeout).\
            until(lambda driver: self.is_element_present(path))
        elem = driver.find_element_by_xpath(path)
        elem.clear()
        elem.send_keys(endpoint)

        path = "//ul[@class='select2-results']"
        WebDriverWait(self.driver, self.context.page_timeout).\
            until(lambda driver: self.is_element_present(path))

        path = "//span[@class='select2-match']"
        driver.find_element_by_xpath(path).click()

        # wait to see if ask passwd pops up
        WebDriverWait(self.driver, 2).\
            until(lambda driver: self.is_authorized(username, password, timeout))

        path = "id('fbrowser')/tbody/tr/td/span[text()='%s']" % globus_dir
        WebDriverWait(self.driver, self.context.page_timeout).\
            until(lambda driver: self.is_element_present(path))
        driver.find_element_by_xpath(path).click()

        path = "id('fbrowser')/tbody/tr/td/span[text()='%s']" % globus_file
        WebDriverWait(self.driver, self.context.page_timeout).\
            until(lambda driver: self.is_element_present(path))
        driver.find_element_by_xpath(path).click()

        driver.find_element_by_name("runtool_btn").click()

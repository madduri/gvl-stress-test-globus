from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base

class GraphDisplayData(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(GraphDisplayData, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_plots", "proportional venn")
    def run_proportional_venn(self, title, input_file_dicts):
        driver = self.driver

        self.clock_action("proportional venn run btn click", 1001)
        self.switch_to_galaxy_content_frame()
        driver.find_element_by_name("title").send_keys(title)
        if len(input_file_dicts) > 0:
            driver.find_element_by_xpath("id('s2id_autogen1')/a[contains(@class, 'select2-choice')]").click()
            driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + input_file_dicts[0]["input_file"] + "')][last()]").click()

            driver.find_element_by_name("column1").clear()
            driver.find_element_by_name("column1").send_keys(input_file_dicts[0]["col_index"])
            driver.find_element_by_name("asName1").clear()
            driver.find_element_by_name("asName1").send_keys(input_file_dicts[0]["as_name"])
        if len(input_file_dicts) > 1:
            driver.find_element_by_xpath("id('s2id_autogen3')/a[contains(@class, 'select2-choice')]").click()
            driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + input_file_dicts[1]["input_file"] + "')][last()]").click()

            driver.find_element_by_name("column2").clear()
            driver.find_element_by_name("column2").send_keys(input_file_dicts[1]["col_index"])
            driver.find_element_by_name("asName2").clear()
            driver.find_element_by_name("asName2").send_keys(input_file_dicts[1]["as_name"])
        if len(input_file_dicts) > 2:
            Select(driver.find_element_by_name("twoThree|tt")).select_by_visible_text("three")

            driver.find_element_by_xpath("id('s2id_autogen5')/a[contains(@class, 'select2-choice')]").click()
            driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + input_file_dicts[2]["input_file"] + "')][last()]").click()
            driver.find_element_by_name("twoThree|column3").clear()
            driver.find_element_by_name("twoThree|column3").send_keys(input_file_dicts[2]["col_index"])
            driver.find_element_by_name("twoThree|asName3").clear()
            driver.find_element_by_name("twoThree|asName3").send_keys(input_file_dicts[2]["as_name"])

        driver.find_element_by_name("runtool_btn").click()

        # wait for cuff diff to finish
        self.clock_action("proportional venn complete", 1001)
        History(self.context).wait_for_datasets_to_finish()

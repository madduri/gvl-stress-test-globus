import threading
import argparse
import unittest
import json
import sys
import time
import inspect
from selenium_snippets.galaxy import GalaxySnippetContext

#
# LOG_CONFIG = {'version':1,
#               'handlers': {'console': { 'class': 'logging.StreamHandler',
#                                       'level': logging.DEBUG},
#                           'file': {'class': 'logging.FileHandler',
#                                    'filename': 'test.log',
#                                    'level': logging.DEBUG}},
#               'root': {'handlers' : ('console', 'file'), 'level': logging.DEBUG }}
# logging.config.dictConfig(LOG_CONFIG)

DEFAULT_GRID_SERVER = ""
DEFAULT_GALAXY_SERVER = "http://galaxy-tut.genome.edu.au/"
DEFAULT_GALAXY_USERNAME = "testbot"  # = "<username>"
DEFAULT_GALAXY_EMAIL = "testbot@genome.edu.au"  # = "<username>"
DEFAULT_GALAXY_PASSWORD = "gvl_letmein"  # = "<password>"
DEFAULT_JOB_TIMEOUT = 3600  # Maximum time (in seconds) to wait for job to finish execution
DEFAULT_PAGE_TIMEOUT = 60  # Maximum time (in seconds) to wait for page to be ready
DEFAULT_LOG_FILENAME = "gvltest"  # if no log file specified on command line as second argument
DEFAULT_TEST_CLASS = "tests.gvl_test_base.GVLTestBase"  # Default test suite to execute
DEFAULT_TOTAL_THREADS = 1  # If no value specified on command line as first argument

def load_class_from_name(fqcn):
    # Break apart fqcn to get module and classname
    paths = fqcn.split('.')
    modulename = '.'.join(paths[:-1])
    classname = paths[-1]
    # Import the module
    __import__(modulename, globals(), locals(), ['*'])
    # Get the class
    cls = getattr(sys.modules[modulename], classname)
    # Check cls
    if not inspect.isclass(cls):
       raise TypeError("%s is not a class" % fqcn)
    # Return class
    return cls

# # Returns an instances of the test class
def get_test_instance(fqcn, test_context):
    test_class = load_class_from_name(fqcn)
    # instantiate the test class
    obj = test_class(test_context)
    return obj

def run_test(log_file_name, selenium_server, galaxy_server, username, email, password, page_timeout, job_timeout, fqcn):
    test_context = GalaxySnippetContext()
    test_context.selenium_server = selenium_server
    test_context.galaxy_server = galaxy_server
    test_context.galaxy_username = username
    test_context.galaxy_email = email
    test_context.galaxy_password = password
    test_context.page_timeout = page_timeout
    test_context.job_timeout = job_timeout
    test_context.current_timeout = page_timeout
    test_context.initialize()

    gvl_test = get_test_instance(fqcn, test_context)
    suite = unittest.TestSuite()
    suite.addTest(gvl_test)
    result = unittest.TextTestRunner().run(suite)
    fp = open("{0}.log".format(log_file_name), 'w')
    json.dump(gvl_test.get_statistics(), fp)
    if result.errors:
        raise Exception("Stress test failed while executing test: {0}. Exception is: {1}".format(result.errors[0][0], result.errors[0][1]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server", default=DEFAULT_GALAXY_SERVER, help="URL of galaxy server to use. The default is %s" % DEFAULT_GALAXY_SERVER)
    parser.add_argument("-u", "--user", default=DEFAULT_GALAXY_USERNAME, help="Galaxy username. Default is %s" % DEFAULT_GALAXY_USERNAME)
    parser.add_argument("-e", "--email", default=DEFAULT_GALAXY_EMAIL, help="Galaxy login email. Default is %s" % DEFAULT_GALAXY_EMAIL)
    parser.add_argument("-p", "--password", default=DEFAULT_GALAXY_PASSWORD, help="Password for galaxy user. Default is %s" % DEFAULT_GALAXY_PASSWORD)
    parser.add_argument("-l", "--log", default=DEFAULT_LOG_FILENAME, help="Prefix for log files. Default is %s" % DEFAULT_LOG_FILENAME)
    parser.add_argument("-pt", "--ptimeout", default=DEFAULT_PAGE_TIMEOUT, help="Maximum time (in seconds) to wait for page to be ready. Default is %s" % DEFAULT_PAGE_TIMEOUT, type=int)
    parser.add_argument("-jt", "--jtimeout", default=DEFAULT_JOB_TIMEOUT, help="Maximum time (in seconds) to wait for job to finish execution. Default is %s" % DEFAULT_JOB_TIMEOUT, type=int)
    parser.add_argument("-g", "--grid", default=DEFAULT_GRID_SERVER, help="Use remote selenium grid server. e.g. http://115.146.86.155:5555/wd/hub. Default is: %s" % DEFAULT_GRID_SERVER)
    parser.add_argument("-c", "--testclass", default=DEFAULT_TEST_CLASS, help="Execute given test suite. Fully qualified classname should be provided. Default: %s" % DEFAULT_TEST_CLASS)
    parser.add_argument("-b", "--browsers", type=int, default=DEFAULT_TOTAL_THREADS, help="Number of parallel browser threads to execute. Default is %s" % DEFAULT_TOTAL_THREADS)
    args = parser.parse_args()

    threads = []
    for x in range(0, args.browsers):
        logname = "%s%d" % (args.log, x)
        t = threading.Thread(
            target=run_test,
            args=(logname, args.grid, args.server, args.user, args.email, args.password, args.ptimeout, args.jtimeout, args.testclass)
        )
        t.start()
        if x == 0:
            # join the first thread to be sure initial history is created
            t.join()
        else:
            threads.append(t)
        time.sleep(45)

    for t in threads:
        t.join()

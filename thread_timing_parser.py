import sys
import argparse
import string
import os
import urllib2
from threading import Thread

import json

"""
Runs a series of workloads through selenium grid, simulating an increasing number of users each time.
"""
import unittest
import time
from bioblend.cloudman.launch import Bunch
from bioblend.cloudman import CloudManConfig
from bioblend.cloudman import CloudManInstance
import gvl_test_runner


def parse_results():
    files_in_dir = []
    result_list = []
    for (dirpath, dirnames, filenames) in os.walk("."):
        files_in_dir.extend(filenames)
        break

    for filename in files_in_dir:
        if "GVL-" in filename:
            result_list.append(filename)

    if result_list:
        print "machine_type,galaxy_data,galaxy_index,workload,users,workers,thread_no,start_time, end_time,time,errors"
        for result in result_list:
            data = result.split("_")
            machine_type = data[1]
            galaxy_data = data[2]
            galaxy_index = data[3]
            workload = data[4]
            users = int(data[5][1:])
            workers = int(data[6][1:])
            thread_no = int(data[7].replace(".log", ""))
            with open(result) as data_file:
                timing = json.load(data_file)

            if not timing:
                continue
            start_time = float(timing['timing'][0]['action_start'])
            end_time = timing['timing'][0]['action_end']

            if end_time:
                total_time = end_time - start_time

            errors = timing['errors']

            print "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}".format(machine_type, galaxy_data, galaxy_index, workload, users, workers, thread_no, start_time, end_time, total_time, errors)


if __name__ == "__main__":
#     parser = argparse.ArgumentParser()
#     parser.add_argument("-m", "--machine", type=str, default=DEFAULT_MACHINE_TYPE, help="Instance type/flavor to use (e.g. m1.small, m1.medium etc). Default is %s" % DEFAULT_MACHINE_TYPE)
#     parser.add_argument("-w", "--workers", type=int, default=DEFAULT_NUM_WORKERS, help="Maximum number of workers to test. Default is %s" % DEFAULT_NUM_WORKERS)
#     parser.add_argument("-d", "--galaxydata", type=str, default=DEFAULT_GALAXY_DATA_STORAGE, help="Storage to use for galaxy data. Should be either transient, or custom-size. Default is %s" % DEFAULT_GALAXY_DATA_STORAGE)
#     parser.add_argument("-idx", "--galaxyindices", type=str, default=DEFAULT_GALAXY_INDEX_STORAGE, help="Storage to use for galaxy index. Should be either volume or gluster. Default is %s" % DEFAULT_GALAXY_INDEX_STORAGE)
#     parser.add_argument("-z", "--zone", type=str, default=DEFAULT_PLACEMENT_ZONE, help="Placement zone to use. Default is %s" % DEFAULT_PLACEMENT_ZONE)
#     args = parser.parse_args()

    parse_results()

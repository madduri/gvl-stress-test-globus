import os
import time
import logging

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import NodeLocation

import libcloud.security

log = logging.getLogger(__name__)

libcloud.security.VERIFY_SSL_CERT = False

OpenStack = get_driver(Provider.OPENSTACK)

driver = OpenStack(os.environ['OS_USERNAME'], os.environ['OS_PASSWORD'],
                   ex_force_auth_version='2.0_password',
                   ex_force_auth_url='https://keystone.rc.nectar.org.au:5000',
                   ex_tenant_name=os.environ['OS_TENANT_NAME'],
                   ex_force_service_name='Compute Service',
                   ex_force_service_region='Melbourne')

class Bunch:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

def create_instance(flavour, zone, instance_name, image_name='NeCTAR Ubuntu 14.04 (Trusty) amd64', wait_till_ready=True):
    images = driver.list_images()
    sizes = driver.list_sizes()
    ubuntu_image = [img for img in images if img.name is not None and image_name in img.name][0]
    machine_size = [size for size in sizes if size.name is not None and flavour == size.name][0]

    node = driver.create_node(name=instance_name, image=ubuntu_image, size=machine_size, ex_keyname='cloudman_key_pair', ex_availability_zone=zone, ex_security_groups=[Bunch(name="CloudMan"), Bunch(name="GVL-Services"), Bunch(name="GVL-Services2")])
    if wait_till_ready:
        node = driver.wait_until_running([node])[0][0]
    return node

def create_instances(flavour, zone, instance_name, instance_count, image_name='NeCTAR Ubuntu 14.04 (Trusty) amd64', wait_till_ready=True):
    instance_list = []
    for x in xrange(1, instance_count + 1):
        instance_list.append(create_instance(flavour, zone, instance_name.format(x), image_name, wait_till_ready=False))
    if wait_till_ready:
        status_list = driver.wait_until_running(instance_list)
        instance_list = [item[0] for item in status_list]
    return instance_list

def attach_volume(instance, volume_name, volume_size, zone):
    driver.create_volume(size=volume_size, name=volume_name, location=zone)
    log.info("Sleeping a bit till volume comes online")
    time.sleep(20)
    volumes = driver.list_volumes()
    volume = [vol for vol in volumes if vol.name is not None and vol.name == volume_name and vol.size == volume_size][0]
    driver.attach_volume(node=instance, volume=volume, device=None)

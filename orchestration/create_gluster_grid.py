import time
import os
import boto
import argparse
import logging
from fabric.api import *
from fabric.tasks import execute

import deploy_gluster
import create_nodes as nodectrl

LOG_CONFIG = {'version':1,
              'formatters': {
                            'default_formatter' : { 'format': '%(asctime)s : %(levelname)s : %(message)s' }
                            },
              'handlers': {'console': { 'class': 'logging.StreamHandler',
                                      'level': logging.DEBUG},
                          'file': {'class': 'logging.FileHandler',
                                   'filename': 'create_node.log',
                                   'formatter': 'default_formatter',
                                   'level': logging.DEBUG}},
              'root': {'handlers' : ('console',), 'level': logging.DEBUG }}
logging.config.dictConfig(LOG_CONFIG)
log = logging.getLogger(__name__)

def create_gluster_grid(node_flavour, zone, instance_prefix, instance_count, volume_size):
    log.debug("Creating gluster peer instances...")
    instances = nodectrl.create_instances(node_flavour, zone, instance_prefix + "-peer{0}", instance_count)
    for idx, instance in enumerate(instances):
        nodectrl.attach_volume(instance, "{0}-vol{1}".format(instance_prefix, idx), volume_size, zone)
    log.debug("Sleeping a bit till the ssh services come up...")
    time.sleep(20)  # Sleep a little till the ssh services come up
    log.debug("Deploying software...")
    deploy_gluster.deploy_gluster_grid([instance.private_ips[0] for instance in instances])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nodes', type=int, help="Number of nodes in grid", required=True)
    parser.add_argument('-k', '--keyfile', type=str, help="Keyfile location", required=True)
    parser.add_argument('-z', '--zone', type=str, help="Placement zone to create nodes", required=True)
    parser.add_argument('-f', '--flavour', type=str, help="Flavour of nodes", required=True)
    parser.add_argument('-p', '--prefix', type=str, help="Name prefix of grid.", required=True)
    parser.add_argument('-s', '--size', type=int, help="Volume size of each peer node", required=True)
    args = parser.parse_args()

    env.user = "ubuntu"
    env.disable_known_hosts = True
    env.key_filename = args.keyfile
    create_gluster_grid(args.flavour, args.zone, args.prefix, args.nodes, args.size)

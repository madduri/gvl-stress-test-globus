import time
import os
import boto
import argparse
from fabric.api import *
from fabric.tasks import execute

@parallel
def install_common():
    sudo('apt-get update')
    sudo('apt-get install default-jre -y')
    sudo("apt-get install tmux -y")

@parallel
def deploy_phantomjs_nodes(head):
    sudo("mkdir -p /gvl")
    sudo("chown ubuntu /gvl")
    run("mkdir -p /gvl/phantomjs")
    with cd('/gvl/phantomjs'):
        run("wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.7-linux-x86_64.tar.bz2")
        run("tar -xf phantomjs-1.9.7-linux-x86_64.tar.bz2 --strip-components=1")
        with cd('/gvl/phantomjs/bin'):
#             run("tmux ls | awk '{print $1}' | sed 's/://g' | xargs -I{} tmux kill-session -t {}")
            run("tmux new -d -s phanjs")
            for i in xrange(1, 7):
                run("tmux send -t phanjs.0 $'./phantomjs --webdriver-selenium-grid-hub=http://{0}:30000 --webdriver={1}:{2} > phantom{3}.log 2>&1 &\n'".format(head, env.host, 30000 + i, i))

@parallel
def deploy_selenium_nodes(head):
    sudo("apt-get install xvfb -y")
    sudo("apt-get install firefox -y")
    sudo("mkdir -p /gvl")
    sudo("chown ubuntu /gvl")
    run("mkdir -p /gvl/selenium")
    with cd('/gvl/selenium'):
        run("wget http://selenium-release.storage.googleapis.com/2.41/selenium-server-standalone-2.41.0.jar")
        run("tmux new -d -s xvfb")
        run("tmux send -t xvfb.0 $'sudo Xvfb :10 -ac\n'")
        run("tmux new -d -s sel")
        run("tmux send -t sel.0 $'export DISPLAY=:10\n'")
        run("tmux send -t sel.0 $'java -jar /gvl/selenium/selenium-server-standalone-2.41.0.jar -role node -port 30000 -nodeTimeout 120 -maxSession 5 -browserTimeout 120 -hub http://{0}:30000/grid/register > seleniumNode.log 2>&1\n'".format(head))

def deploy_selenium_hub():
    sudo("mkdir -p /gvl")
    sudo("chown ubuntu /gvl")
    run("mkdir -p /gvl/selenium")
    with cd('/gvl/selenium'):
        run("wget http://selenium-release.storage.googleapis.com/2.41/selenium-server-standalone-2.41.0.jar")
        run("tmux new -d -s sel")
        run("tmux send -t sel.0 $'java -jar /gvl/selenium/selenium-server-standalone-2.41.0.jar -role hub -port 30000 -nodeTimeout 120 -maxSession 30 -browserTimeout 120 > selenium.log 2>&1\n'")

def deploy_selenium_grid(head, nodes):
    execute(install_common, hosts=[head] + nodes)
    execute(deploy_selenium_hub, hosts=head)
    execute(deploy_selenium_nodes, head, hosts=nodes)
#     execute(deploy_phantomjs_nodes, head, hosts=nodes)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--gridhub", type=str, help="Selenium grid head node", required=True)
    parser.add_argument('-n', '--nodes', nargs='+', type=str, help="PhantomJS nodes", required=True)
    parser.add_argument('-k', '--keyfile', type=str, help="Keyfile location", required=True)
    args = parser.parse_args()

    env.user = "ubuntu"
    env.disable_known_hosts = True
    env.key_filename = args.keyfile
    deploy_selenium_grid(args.gridhub, args.nodes)



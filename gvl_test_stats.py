import json
import argparse

DEFAULT_LOG_FILENAME = "gvltest"  # if no log file specified on command line as second argument

class GVLTestStats():

    def print_stats(self, log_data, error_count, total, slowest, slow_percentage):
        print "\n-----------------------"
        print "GVL Stress Test Results"
        print "-----------------------"

        for index, run in enumerate(log_data):
            self.print_timing(index, run)

    def load_log_data(self, log_file):
        log_data = []
        count = 0
        while True:
            filename = "{0}{1}.log".format(log_file_name, count)
            try:
                fp = open(filename, 'r')
                log_data.append(json.load(fp))
            except Exception, e:
                break
            count += 1
        return log_data

    def process_stats(self, log_file):
        log_data = self.load_log_data(log_file)

        for index, run in enumerate(log_data):
            self.print_timing(index, run['timing'])

    def print_timing(self, index, actions):
        for action in actions:
            print '%d,"%s",%f' % (index, action['action_name'], action['action_time'])
            child_actions = action.get('child_actions')
            if child_actions is not None:
                self.print_timing(index, child_actions)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--log", default=DEFAULT_LOG_FILENAME, help="Prefix for log files. Default is %s" % DEFAULT_LOG_FILENAME)
    args = parser.parse_args()
    log_file_name = args.log

    assert(log_file_name)
    stat_printer = GVLTestStats()
    stat_printer.process_stats(log_file_name)
